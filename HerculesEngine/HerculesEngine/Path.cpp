#include "Path.h"

using namespace HerculesEngine;
using namespace HerculesEngine::Tools;
using namespace std;

Path::Path()
{
	//INTENTIONALLY EMPTY
}

Path::Path(const std::string& path) : m_path(path)
{
	transform();
}

Path::Path(const char* path) : m_path(path)
{
	transform();
}

void Path::operator=(const char* path)
{
	m_path = path;
	transform();
}

void Path::operator=(const std::string& path)
{
	m_path = path;
	transform();
}

void Path::transform()
{
	for (auto& character : m_path)
	{
		if (character == INCORRECT_SLASH)
		{
			character = CORRECT_SLASH;
		}
	}
}

Path::operator std::string()
{
	return m_path;
}

const char* Path::c_str() const
{
	return m_path.c_str();
}

Path::~Path()
{
	//INTENTIONALLY EMPTY
}