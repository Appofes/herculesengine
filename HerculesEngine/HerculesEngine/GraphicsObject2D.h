#ifndef __GraphicsObject2DH__
#define __GraphicsObject2DH__

#include "Effect.h"
#include <glm.hpp>
#include <vector>
#include <memory>

namespace HerculesEngine
{
	namespace Graphics
	{
		class GraphicsObject2D
		{
		public:
			virtual ~GraphicsObject2D();

			virtual void draw() = 0;
			virtual void update() = 0;
			virtual void setModelMatrix(const glm::mat4x4& modelMatrix);
			virtual void getModelMatrix(glm::mat4x4* modelMatrix) const;
		protected:
			glm::mat4x4 m_modelMatrix;
			Tools::Effect m_effect;
			unsigned int m_vertexArrayObjectID;
			unsigned int m_vertexBufferObjectID;
			unsigned int m_indexBufferObjectID;
		};
	}
}

#endif//__GraphicsObject2DH__