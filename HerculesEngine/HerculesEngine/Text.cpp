#include "Text.h"
#include "Vertex.h"
#include "Core.h"
#include <vector>
using namespace std;

using namespace HerculesEngine;
using namespace HerculesEngine::Graphics;

float Text::m_sx;
float Text::m_sy;

Text::Text()
{
	//INTENTIONALLY EMPTY
}

Text::Text(const std::string& font,
	const int fontSizeInPixels[2],
	const Tools::Effect& effect,
	const glm::vec2& origin,
	const glm::vec4& color)
	//TODO: add color parameter
{
	m_effect = effect;

	m_color = glm::vec4(0, 0, 0, 1);
	m_origin = origin;

	m_colorLocation = m_effect.getVariableUniformLocation("g_color");
	m_textureLocation = m_effect.getVariableUniformLocation("g_texture");

	/*std::vector<Graphics::VertexTextured> vertices =
	{
		{ { -0.5f, -0.5f, 0.51f, 1.0f }, { 0.0f, 1.0f } },
		{ { -0.5f, 0.25f, 0.51f, 1.0f }, { 0.0f, 0.0f } },
		{ { 0.5f, 0.25f, 0.51f, 1.0f }, { 1.0f, 0.0f } },
		{ { 0.5f, -0.5f, 0.51f, 1.0f }, { 1.0f, 1.0f } }
	};

	vector<GLuint> indices =
	{
		0, 1, 2,
		0, 2, 3
	};
	
	glGenVertexArrays(1, &m_vertexArrayObjectID);
	glBindVertexArray(m_vertexArrayObjectID);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &m_vertexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObjectID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (GLvoid*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (GLvoid*)sizeof(vertices[0].m_position));

	glGenBuffers(1, &m_indexBufferObjectID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferObjectID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);*/

	glGenTextures(1, &m_textureID);

	glBindTexture(GL_TEXTURE_2D, m_textureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);

	static FT_Library ft = nullptr;

	if (ft == nullptr)
	{
		FT_Init_FreeType(&ft);
		//TODO: Check errors
		CORE::getOnResizeEvent() += onResize;
	}

	FT_New_Face(ft, font.c_str(), 0, &m_face);
	//TODO: Check errors

	FT_Set_Pixel_Sizes(m_face, fontSizeInPixels[0], fontSizeInPixels[1]);

	m_glyph = m_face->glyph;
}

Text::~Text()
{
	glDeleteTextures(1, &m_textureID);

	glDeleteBuffers(1, &m_vertexBufferObjectID);
	glDeleteBuffers(1, &m_indexBufferObjectID);
	glDeleteVertexArrays(1, &m_vertexArrayObjectID);
}

void Text::setText(const std::string& text)
{
	m_text = text;
}

const std::string& Text::getText() const
{
	return m_text;
}

void Text::draw()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	m_effect.enable();

	auto origin = m_origin;

	glUniform4fv(m_colorLocation, 1, glm::value_ptr(m_color));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_textureID);

	glUniform1i(m_textureLocation, 0);

	for (auto character : m_text)
	{
		FT_Load_Char(m_face, character, FT_LOAD_RENDER);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			m_glyph->bitmap.width,
			m_glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			m_glyph->bitmap.buffer);


		float x = origin.x + m_glyph->bitmap_left * m_sx;
		float y = -origin.y - m_glyph->bitmap_top * m_sy;
		float w = m_glyph->bitmap.width * m_sx;
		float h = m_glyph->bitmap.rows * m_sy;
		
		std::vector<Graphics::VertexScreenTextured> vertices =
		{
			{ { x, -y }, { 0.0f, 0.0f } },
			{ { x + w, -y }, { 1.0f, 0.0f } },
			{ { x, -y - h }, { 0.0f, 1.0f } },
			{ { x + w, -y - h }, { 1.0f, 1.0f } }
		};

		vector<GLuint> indices =
		{
			0, 1, 2,
			2, 1, 3
		};

		glGenVertexArrays(1, &m_vertexArrayObjectID);
		glBindVertexArray(m_vertexArrayObjectID);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glGenBuffers(1, &m_vertexBufferObjectID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObjectID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (GLvoid*)0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (GLvoid*)sizeof(vertices[0].m_position));

		glGenBuffers(1, &m_indexBufferObjectID);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferObjectID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), indices.data(), GL_STATIC_DRAW);

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)0);

		origin.x += (m_glyph->advance.x >> 6) * m_sx;
		origin.y += (m_glyph->advance.y >> 6) * m_sy;
	}

	m_effect.disable();

	glBindVertexArray(0);

	glDisable(GL_BLEND);
}

void Text::update()
{
	
}

void Text::onResize(int width, int height)
{
	m_sx = 2.0f / width;
	m_sy = 2.0f / height;
}