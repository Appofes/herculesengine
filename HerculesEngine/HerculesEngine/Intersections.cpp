#include "Intersections.h"

void HerculesEngine::Tools::GetPickRayLocal(
	HerculesEngine::Tools::Ray* ray,
	const glm::tvec2<int>& point,
	int screenWidth,
	int screenHeight,
	const glm::mat4x4& model,
	const glm::mat4x4& view,
	const glm::mat4x4& proj)
{
	ray->m_origin = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	ray->m_direction.x = (((2.0f * point.x) / screenWidth) - 1.0f) / proj[0][0];
	ray->m_direction.y = (((-2.0f * point.y) / screenHeight) + 1.0f) / proj[1][1];
	ray->m_direction.z = -1.0f;
	ray->m_direction.w = 0.0f;
	
	auto to_local = glm::inverse(model) * glm::inverse(view);

	ray->m_direction = glm::normalize(to_local * ray->m_direction);
	ray->m_origin = to_local * ray->m_origin;
}

bool HerculesEngine::Tools::LocalRayTriangleIntersectionTest(const Ray& ray, const glm::vec4 triangle[3])
{
	auto e1 = glm::vec3(triangle[1] - triangle[0]);
	auto e2 = glm::vec3(triangle[2] - triangle[0]);
	auto m = glm::vec3(ray.m_origin - triangle[0]);
	auto d = glm::vec3(ray.m_direction);

	auto denominator = glm::dot(e1, glm::cross(d, e2));

	auto u = glm::dot(m, glm::cross(d, e2)) / denominator;

	if (u < 0)
	{
		return false;
	}

	auto v = glm::dot(d, glm::cross(m, e1)) / denominator;

	if (v < 0 || (1 < (u + v)))
	{
		return false;
	}

	return true;
}