#ifndef __VertexH__
#define __VertexH__

#include <glm.hpp>

namespace HerculesEngine
{
	namespace Graphics
	{
		struct VertexTextured
		{
			glm::vec4 m_position;
			glm::vec2 m_texCoord;
		};

		struct VertexScreenTextured
		{
			glm::vec2 m_position;
			glm::vec2 m_texCoord;
		};
	}
}

#endif//__VertexH__