#ifndef __PathH__
#define __PathH__

#include <string>

namespace HerculesEngine
{
	namespace Tools
	{
		class Path 
		{
		public:
			Path();
			Path(const std::string& path);
			Path(const char* path);
			~Path();
		
			void operator=(const char* path);
			void operator=(const std::string& path);
					
			void transform();
					
			operator std::string();
		
			const char* c_str() const;
		private:
		#ifdef _WIN32
			const char CORRECT_SLASH = '\\';
			const char INCORRECT_SLASH = '/';
		#elif __linux
			const char CORRECT_SLASH = '/';
			const char INCORRECT_SLASH = '\\';
		#endif
			std::string m_path;
		};
	}
}

#endif//__PathH__