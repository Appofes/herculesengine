#ifndef __LayerH__
#define __LayerH__

#include "GraphicsObject2D.h"
#include "LayerElement.h"

namespace HerculesEngine
{
	namespace GUI
	{
		class Layer : public Graphics::GraphicsObject2D, public sigslot::has_slots<>
		{
		public:
			Layer(const std::vector<LayerElement*>& elements);
			virtual ~Layer();

			void draw();
			void update();

		private:
			void onActiveMouseMove(int x, int y);
			void onPassiveMouseMove(int x, int y);
			void onMouseButtonPress(int button, int x, int y);
			void onMouseButtonRelease(int button, int x, int y);
			void onMouseWheelMove(int direction, int x, int y);
			void onAction(ACTION_DESC desc, const Action& action);
		private:
			std::vector<LayerElement*> m_elements;
			sigslot::signal2<ACTION_DESC, const Action&> m_onAction;
			friend class Menu;
		};
	}
}

#endif//__LayerH__