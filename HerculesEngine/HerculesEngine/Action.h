#ifndef __ActionH__
#define __ActionH__

namespace HerculesEngine
{
	namespace GUI
	{
		enum class ACTION_DESC { JUMP };

		struct Action
		{
			void* m_jumpDestination;
		};
	}
}

#endif//__ActionH__