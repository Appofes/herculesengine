#include "GraphicsObject2D.h"

using namespace HerculesEngine::Graphics;

void GraphicsObject2D::setModelMatrix(const glm::mat4x4& modelMatrix)
{
	m_modelMatrix = modelMatrix;
}

void GraphicsObject2D::getModelMatrix(glm::mat4x4* modelMatrix) const
{
	*modelMatrix = m_modelMatrix;
}

GraphicsObject2D::~GraphicsObject2D()
{
	//INTENTIONALLY EMPTY
}