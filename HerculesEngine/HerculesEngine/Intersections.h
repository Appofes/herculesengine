#ifndef __IntersectionsH__
#define __IntersectionsH__

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>

namespace HerculesEngine
{
	namespace Tools
	{
		struct Ray
		{
			glm::vec4 m_origin;
			glm::vec4 m_direction;
		};

		void GetPickRayLocal(
			Ray* ray,
			const glm::tvec2<int>& screenPoint,
			int screenWidth,
			int screenHeight,
			const glm::mat4x4& model,
			const glm::mat4x4& view,
			const glm::mat4x4& proj);

		bool LocalRayTriangleIntersectionTest(const Ray& ray, const glm::vec4 triangle[3]);
	}
}

#endif//__IntersectionsH__