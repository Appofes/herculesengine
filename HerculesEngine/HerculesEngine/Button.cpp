#include "Button.h"
#include "Core.h"
#include "Intersections.h"

using namespace HerculesEngine;
using namespace HerculesEngine::GUI;

Button::Button(const glm::vec3& position,
	const std::vector<Graphics::VertexTextured>& vertices,
	const std::vector<unsigned int>& indices,
	const std::vector<std::pair<std::string, LAYER_ELEMENT_STATE>>& textures,
	const Tools::Effect& effect) : LayerElement(position, vertices, indices, textures, effect)
{
	m_projViewModelMatrixLocation = m_effect.getVariableUniformLocation("g_projViewModel");
	m_currentState = LAYER_ELEMENT_STATE::DEFAULT;
	m_textureLocation = m_effect.getVariableUniformLocation("g_texture");
}

Button::~Button()
{
	//INTENTIONALLY EMPTY
}

void Button::draw()
{
	m_effect.enable();

	auto proj_view_model = CORE::getProjectionMatrix() * CORE::getViewMatrix() * m_modelMatrix;
	glUniformMatrix4fv(m_projViewModelMatrixLocation, 1, GL_FALSE, glm::value_ptr(proj_view_model));

	glActiveTexture(GL_TEXTURE0);

	for (auto& texture : m_textures)
	{
		if (m_currentState == texture.second)
		{
			glBindTexture(GL_TEXTURE_2D, texture.first);
			break;
		}
	}

	glUniform1i(m_textureLocation, 0);

	glBindVertexArray(m_vertexArrayObjectID);

	glDrawElements(GL_TRIANGLES, m_numIndices, GL_UNSIGNED_INT, (GLvoid*)0);

	glBindVertexArray(0);

	m_effect.disable();
}

void Button::update()
{
	
}

void Button::setAction(ACTION_DESC desc, const Action& action)
{
	switch (desc)
	{
	case ACTION_DESC::JUMP:
		m_action.m_jumpDestination = action.m_jumpDestination;
		break;
	}
}

void Button::onPassiveMouseMove(int x, int y)
{
	Tools::Ray pick_ray;
	glm::vec4 triangle[3];

	for (int i = 0; i < m_indices.size(); i += 3)
	{
		Tools::GetPickRayLocal(
			&pick_ray,
			{ x, y },
			CORE::getClientWindowWidth(),
			CORE::getClientWindowHeight(),
			m_modelMatrix,
			CORE::getViewMatrix(),
			CORE::getProjectionMatrix()
			);

		triangle[0] = m_vertices[m_indices[i]].m_position;
		triangle[1] = m_vertices[m_indices[i + 1]].m_position;
		triangle[2] = m_vertices[m_indices[i + 2]].m_position;

		bool focused = Tools::LocalRayTriangleIntersectionTest(pick_ray, triangle);

		if (focused)
		{
			m_currentState = LAYER_ELEMENT_STATE::FOCUSED;
			return;
		}
	}

	m_currentState = LAYER_ELEMENT_STATE::DEFAULT;
}

void Button::onActiveMouseMove(int x, int y)
{
	printf("Button::onActiveMouseMove\n");
}

void Button::onMouseButtonPress(int button, int x, int y)
{
	//printf("Button::onMouseButtonPress\n");
	if (m_currentState == LAYER_ELEMENT_STATE::FOCUSED && button == GLUT_LEFT_BUTTON)
	{
		m_currentState = LAYER_ELEMENT_STATE::PRESSED;
		m_onAction(ACTION_DESC::JUMP, m_action);
	}
}

void Button::onMouseButtonRelease(int button, int x, int y)
{
	printf("Button::onMouseButtonRelease\n");
}

void Button::onMouseWheelMove(int direction, int x, int y)
{
	printf("Button::onMouseWheelMove\n");
}