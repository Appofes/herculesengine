#ifndef __KeyboardH__
#define __KeyboardH__

#include "Event.h"
#include <vector>
#include <map>

#define KEYBOARD HerculesEngine::IO::Keyboard

namespace HerculesEngine
{
	namespace IO
	{
		class Keyboard
		{
		public:
			Keyboard() = delete;
			virtual ~Keyboard();

			Keyboard(const Keyboard& copyingKeyboard) = delete;
			void operator=(const Keyboard& copyingKeyboard) = delete;

			Keyboard(const Keyboard&& movingKeyboard) = delete;
			void operator=(const Keyboard&& movingKeyboard) = delete;

			static void initialize();

			static Tools::Event<void, int, int, int>& getOnKeyPressEvent();
			static Tools::Event<void, int, int, int>& getOnKeyReleaseEvent();
			static bool isKeyPressed(int key);
		private:
			static void onKeyPress(unsigned char key, int x, int y);
			static void onKeyRelease(unsigned char key, int x, int y);
			static void onSpecialKeyPress(int key, int x, int y);
			static void onSpecialKeyRelease(int key, int x, int y);
		private:
			static Tools::Event<void, int, int, int> m_onKeyPress;
			static Tools::Event<void, int, int, int> m_onKeyRelease;
			
			static const int NUMBER_OF_KEYS;
			static std::vector<bool> m_keyStates;
			static std::map<int, int> m_specialKeysIndices;
		};
	}
}

#endif//__KeyboardH__