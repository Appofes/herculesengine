#ifndef __ButtonH__
#define __ButtonH__

#include "LayerElement.h"

namespace HerculesEngine
{
	namespace GUI
	{
		class Button : public LayerElement
		{
		public:
			Button(const glm::vec3& position,
				const std::vector<Graphics::VertexTextured>& vertices,
				const std::vector<unsigned int>& indices,
				const std::vector<std::pair<std::string, LAYER_ELEMENT_STATE>>& textures,
				const Tools::Effect& effect);
			~Button();

			void draw();
			void update();

			void setAction(ACTION_DESC desc, const Action& action);
		protected:
			void onPassiveMouseMove(int x, int y);
			void onActiveMouseMove(int x, int y);
			void onMouseButtonPress(int button, int x, int y);
			void onMouseButtonRelease(int button, int x, int y);
			void onMouseWheelMove(int direction, int x, int y);
		private:
			LAYER_ELEMENT_STATE m_currentState;
			unsigned int m_projViewModelMatrixLocation;
			unsigned int m_textureLocation;
		};
	}
}

#endif//__ButtonH__