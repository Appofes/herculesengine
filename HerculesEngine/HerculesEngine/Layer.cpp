#include "Layer.h"

using namespace HerculesEngine;
using namespace HerculesEngine::GUI;

Layer::Layer(const std::vector<LayerElement*>& elements)
	: m_elements(elements)
{
	for (auto& element : elements)
	{
		element->m_onAction.connect(this, &Layer::onAction);
	}
}

Layer::~Layer()
{
	//INTENTIONALLY EMPTY
}

void Layer::draw()
{
	for (auto& element : m_elements)
	{
		element->draw();
	}
}

void Layer::update()
{
	for (auto& element : m_elements)
	{
		element->update();
	}
}

void Layer::onActiveMouseMove(int x, int y)
{
	for (auto& element : m_elements)
	{
		element->onActiveMouseMove(x, y);
	}
}

void Layer::onPassiveMouseMove(int x, int y)
{
	for (auto& element : m_elements)
	{
		element->onPassiveMouseMove(x, y);
	}
}

void Layer::onMouseButtonPress(int button, int x, int y)
{
	for (auto& element : m_elements)
	{
		element->onMouseButtonPress(button, x, y);
	}
}

void Layer::onMouseButtonRelease(int button, int x, int y)
{
	for (auto& element : m_elements)
	{
		element->onMouseButtonRelease(button, x, y);
	}
}

void Layer::onMouseWheelMove(int direction, int x, int y)
{
	for (auto& element : m_elements)
	{
		element->onMouseWheelMove(direction, x, y);
	}
}

void Layer::onAction(ACTION_DESC desc, const Action& action)
{
	printf("1\n");

	switch (desc)
	{
	case ACTION_DESC::JUMP:
		//__raise m_onAction(desc, action);
		m_onAction(desc, action);
		break;
	}
}

//Tools::Event<void, class Menu, ACTION_DESC, const Action&>& Layer::getOnActionEvent()
//{
//	return m_onAction;
//}