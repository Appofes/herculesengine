#include "Core.h"
#include "Resources.h"

unsigned int HerculesEngine::Tools::CreateTexture2D(const std::string& filename, int minFilter, int magFilter)
{
	int width = 0;
	int height = 0;
	int comp = 0;

	auto image = stbi_load(filename.c_str(), &width, &height, &comp, STBI_rgb_alpha);

	if (image == nullptr)
	{
		//TODO: throw file error exception
	}

	unsigned int texture_ID = 0;
	glGenTextures(1, &texture_ID);

	glBindTexture(GL_TEXTURE_2D, texture_ID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);

	glBindTexture(GL_TEXTURE_2D, 0);

	stbi_image_free(image);

	return texture_ID;
}