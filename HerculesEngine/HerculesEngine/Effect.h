#ifndef __EffectH__
#define __EffectH__

#include <string>

namespace HerculesEngine
{
	namespace Tools
	{
		class Effect
		{
		public:
			Effect();
			Effect(const std::string& vertexShaderFilename, const std::string& fragmentShaderFilename);
			virtual ~Effect();

			unsigned int getID() const;
			unsigned int getVertexShaderID() const;
			unsigned int getFragmentShaderID() const;
			unsigned int getVariableUniformLocation(const std::string& variableName) const;

			void enable() const;
			void disable() const;

			static unsigned int loadShader(const std::string& filename, unsigned int shaderType);
		private:
			unsigned int m_ID;
			unsigned int m_vertexShaderID;
			unsigned int m_fragmentShaderID;
		};
	}
}

#endif//__EffectH__