#ifndef __MenuH__
#define __MenuH__

#include <stdio.h>
#include "Layer.h"
#include "Mouse.h"

namespace HerculesEngine
{
	namespace GUI
	{
		class Menu : public sigslot::has_slots<>
		{
		public:
			Menu(const std::vector<Layer*>& layers);
			virtual ~Menu();

			void makeActive();
			void makeInactive();
			bool isActive();

			void draw();
			void update();
		private:
			static void onActiveMouseMoveHandler(int x, int y);
			static void onPassiveMouseMoveHandler(int x, int y);
			static void onMouseButtonPressHandler(int button, int x, int y);
			static void onMouseButtonReleaseHandler(int button, int x, int y);
			static void onMouseWheelMoveHandler(int direction, int x, int y);

			void onActiveMouseMove(int x, int y);
			void onPassiveMouseMove(int x, int y);
			void onMouseButtonPress(int button, int x, int y);
			void onMouseButtonRelease(int button, int x, int y);
			void onMouseWheelMove(int direction, int x, int y);
		
			void onAction(ACTION_DESC desc, const Action& action);
		private:
			static sigslot::signal2<int, int> m_onActiveMouseMove;
			static sigslot::signal2<int, int> m_onPassiveMouseMove;
			static sigslot::signal3<int, int, int> m_onMouseButtonPress;
			static sigslot::signal3<int, int, int> m_onMouseButtonRelease;
			static sigslot::signal3<int, int, int> m_onMouseWheelMove;

			std::vector<Layer*> m_layers;
			Layer* m_activeLayer;
			bool m_active;
		};
	}
}



#endif//__MenuH__