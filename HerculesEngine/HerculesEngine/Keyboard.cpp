#include "Keyboard.h"
#include "Core.h"

using namespace HerculesEngine;
using namespace HerculesEngine::IO;
using namespace HerculesEngine::Tools;
using namespace std;

const int Keyboard::NUMBER_OF_KEYS = 286;
std::vector<bool> Keyboard::m_keyStates;
std::map<int, int> Keyboard::m_specialKeysIndices;
Event<void, int, int, int> Keyboard::m_onKeyPress;
Event<void, int, int, int> Keyboard::m_onKeyRelease;

void Keyboard::initialize()
{
	static bool first_time = true;

	if (first_time == true)
	{
		glutKeyboardFunc(onKeyPress);
		glutKeyboardUpFunc(onKeyRelease);
		glutSpecialFunc(onSpecialKeyPress);
		glutSpecialUpFunc(onSpecialKeyRelease);

		glutIgnoreKeyRepeat(GL_TRUE);

		m_specialKeysIndices.insert({ GLUT_KEY_ALT_L, 256 });
		m_specialKeysIndices.insert({ GLUT_KEY_ALT_R, 257 });
		m_specialKeysIndices.insert({ GLUT_KEY_BEGIN, 258 });
		m_specialKeysIndices.insert({ GLUT_KEY_CTRL_L, 259 });
		m_specialKeysIndices.insert({ GLUT_KEY_CTRL_R, 260 });
		m_specialKeysIndices.insert({ GLUT_KEY_DELETE, 261 });
		m_specialKeysIndices.insert({ GLUT_KEY_DOWN, 262 });
		m_specialKeysIndices.insert({ GLUT_KEY_END, 263 });
		m_specialKeysIndices.insert({ GLUT_KEY_F1, 264 });
		m_specialKeysIndices.insert({ GLUT_KEY_F10, 265 });
		m_specialKeysIndices.insert({ GLUT_KEY_F11, 266 });
		m_specialKeysIndices.insert({ GLUT_KEY_F12, 267 });
		m_specialKeysIndices.insert({ GLUT_KEY_F2, 268 });
		m_specialKeysIndices.insert({ GLUT_KEY_F3, 269 });
		m_specialKeysIndices.insert({ GLUT_KEY_F4, 270 });
		m_specialKeysIndices.insert({ GLUT_KEY_F5, 271 });
		m_specialKeysIndices.insert({ GLUT_KEY_F6, 272 });
		m_specialKeysIndices.insert({ GLUT_KEY_F7, 273 });
		m_specialKeysIndices.insert({ GLUT_KEY_F8, 274 });
		m_specialKeysIndices.insert({ GLUT_KEY_F9, 275 });
		m_specialKeysIndices.insert({ GLUT_KEY_HOME, 276 });
		m_specialKeysIndices.insert({ GLUT_KEY_INSERT, 277 });
		m_specialKeysIndices.insert({ GLUT_KEY_LEFT, 278 });
		m_specialKeysIndices.insert({ GLUT_KEY_NUM_LOCK, 279 });
		m_specialKeysIndices.insert({ GLUT_KEY_PAGE_DOWN, 280 });
		m_specialKeysIndices.insert({ GLUT_KEY_PAGE_UP, 281 });
		m_specialKeysIndices.insert({ GLUT_KEY_RIGHT, 282 });
		m_specialKeysIndices.insert({ GLUT_KEY_SHIFT_L, 283 });
		m_specialKeysIndices.insert({ GLUT_KEY_SHIFT_R, 284 });
		m_specialKeysIndices.insert({ GLUT_KEY_UP, 285 });
		
		m_keyStates = vector<bool>(NUMBER_OF_KEYS, false);

		first_time = false;
	}
}

Event<void, int, int, int>& Keyboard::getOnKeyPressEvent()
{
	return m_onKeyPress;
}

Event<void, int, int, int>& Keyboard::getOnKeyReleaseEvent()
{
	return m_onKeyRelease;
}

bool Keyboard::isKeyPressed(int key)
{
	if (key < 256)
	{
		return m_keyStates[key];
	}
	else
	{
		return m_keyStates[m_specialKeysIndices.at(key)];
	}
}

void Keyboard::onKeyPress(unsigned char key, int x, int y)
{
	m_keyStates[key] = true;
	m_onKeyPress(key, x, y);
}

void Keyboard::onKeyRelease(unsigned char key, int x, int y)
{
	m_keyStates[key] = false;
	m_onKeyRelease(key, x, y);
}

void Keyboard::onSpecialKeyPress(int key, int x, int y)
{
	m_keyStates[m_specialKeysIndices.at(key)] = true;
	m_onKeyPress(key, x, y);
}

void Keyboard::onSpecialKeyRelease(int key, int x, int y)
{
	m_keyStates[m_specialKeysIndices.at(key)] = false;
	m_onKeyRelease(key, x, y);
}