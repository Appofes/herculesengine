#version 400

in vec2 ex_texCoordL;
out vec4 pixelColor;

uniform sampler2D g_texture;

void main()
{
	pixelColor = texture(g_texture, ex_texCoordL).rgba;
	//pixelColor = vec4(0.0f, 0.0f, 0.0f, 0.0f);
}