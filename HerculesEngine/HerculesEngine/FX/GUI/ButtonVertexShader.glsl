#version 400

layout (location = 0) in vec4 positionL;
layout (location = 1) in vec2 texCoordL;

out vec2 ex_texCoordL;

uniform mat4 g_projViewModel;

void main()
{
	gl_Position = g_projViewModel * positionL;
	ex_texCoordL = texCoordL;
}