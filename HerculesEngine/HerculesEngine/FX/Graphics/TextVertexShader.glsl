#version 400

layout (location = 0) in vec2 positionL;
layout (location = 1) in vec2 texCoordL;

out vec2 ex_texCoordL;

//uniform mat4 g_projViewModel;

void main()
{
	gl_Position = vec4(positionL.xy, -1.0f, 1);
	ex_texCoordL = texCoordL;
}