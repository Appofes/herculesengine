#version 400

in vec2 ex_texCoordL;
out vec4 pixelColor;

uniform sampler2D g_texture;
uniform vec4 g_color;

void main()
{
	pixelColor = vec4(1, 1, 1, texture2D(g_texture, ex_texCoordL).r) * g_color;
}