#include "Core.h"
#include "Mouse.h"

using namespace HerculesEngine;
using namespace HerculesEngine::IO;

Tools::Event<void, int, int> Mouse::m_onPassiveMove;
Tools::Event<void, int, int> Mouse::m_onActiveMove;
Tools::Event<void, int, int, int> Mouse::m_onButtonPress;
Tools::Event<void, int, int, int> Mouse::m_onButtonRelease;
Tools::Event<void, int, int, int> Mouse::m_onWheelMove;
int Mouse::m_cursorPosition[2];

Mouse::~Mouse()
{
	//INTENTIONALLY EMPTY
}

void Mouse::initialize()
{
	glutMotionFunc(onActiveMove);
	glutPassiveMotionFunc(onPassiveMove);
	glutMouseFunc(onPress);
	glutMouseWheelFunc(onWheelMove);
}

void Mouse::onActiveMove(int x, int y)
{
	m_cursorPosition[0] = x;
	m_cursorPosition[1] = y;

	m_onActiveMove(x, y);
}

void Mouse::onPassiveMove(int x, int y)
{
	m_cursorPosition[0] = x;
	m_cursorPosition[1] = y;

	m_onPassiveMove(x, y);
}

void Mouse::onPress(int button, int state, int x, int y)
{
	m_cursorPosition[0] = x;
	m_cursorPosition[1] = y;

	switch (state)
	{
	case GLUT_UP:
		m_onButtonRelease(button, x, y);
		break;
	case GLUT_DOWN:
		m_onButtonPress(button, x, y);
		break;
	}
}

void Mouse::onWheelMove(int wheel, int direction, int x, int y)
{
	m_onWheelMove(direction, x, y);
}

void Mouse::setCursorPosition(int x, int y)
{
	glutWarpPointer(x, y);

	m_cursorPosition[0] = x;
	m_cursorPosition[1] = y;
}

int Mouse::getCursorPositionX()
{
	return m_cursorPosition[0];
}

int Mouse::getCursorPositionY()
{
	return m_cursorPosition[1];
}

Tools::Event<void, int, int>& Mouse::getOnPassiveMoveEvent()
{
	return m_onPassiveMove;
}

Tools::Event<void, int, int>& Mouse::getOnActiveMoveEvent()
{
	return m_onActiveMove;
}

Tools::Event<void, int, int, int>& Mouse::getOnButtonPressEvent()
{
	return m_onButtonPress;
}

Tools::Event<void, int, int, int>& Mouse::getOnButtonReleaseEvent()
{
	return m_onButtonRelease;
}

Tools::Event<void, int, int, int>& Mouse::getOnWheelMoveEvent()
{
	return m_onWheelMove;
}