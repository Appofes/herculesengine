#ifndef __CoreH__
#define __CoreH__

#include <glew.h>
#include <freeglut.h>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <stb_image.h>
#include "Event.h"
#include "Menu.h"
#include "Text.h"

#define CORE HerculesEngine::Logic::Core

namespace HerculesEngine
{
	namespace Logic
	{
		class Core
		{
		public:
			Core() = delete;
			virtual ~Core();

			Core(const Core& copyingCore) = delete;
			Core& operator=(const Core& copyingCore) = delete;
			Core(const Core&& movingCore) = delete;
			Core& operator=(const Core&& movingCore) = delete;
			
			static void initialize(int numCmdArgs, char* cmdArgs[]);
			static int gameLoop();

			static void pause();
			static void proceed();

			static int getClientWindowWidth();
			static int getClientWindowHeight();
			static bool getPauseState();
			static Tools::Event<void, int, int>& getOnResizeEvent();
			static Tools::Event<void>& getOnDrawEvent();
			static Tools::Event<void, float>& getOnUpdateEvent();
			static const glm::mat4x4& getViewMatrix();
			static const glm::mat4x4& getProjectionMatrix();
		private:
			static void cleanup();
			static void draw();
			static void update();
			static void onResize(int width, int height);
			static void onIdle();
		private:
			static int m_clientWindowSize[2];
			static bool m_paused;
			
			static Tools::Event<void, int, int> m_onResize;
			static Tools::Event<void> m_onDraw;
			static Tools::Event<void, float> m_onUpdate;
			
			static glm::mat4x4 m_projectionMatrix;
			static glm::mat4x4 m_viewMatrix;

			static const int INITIAL_WINDOW_SIZE[2];
			static const int INITIAL_PAUSE_STATE;
			static const float DEFAULT_FOV_IN_DEGREES;
			static const float DEFAULT_FOV_IN_RADIANS;
			static const float DEFAULT_NEAR_PLANE_DISTANCE;
			static const float DEFAULT_FAR_PLANE_DISTANCE;
			static const glm::vec3 DEFAULT_CAMERA_POSITION;

			//static GUI::Button* m_pButton;
			static GUI::Menu* m_pMenu;
			static Graphics::Text* m_text;
		};
	}
}

#endif//__CoreH__