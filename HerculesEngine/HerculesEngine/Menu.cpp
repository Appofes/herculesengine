#include "Menu.h"
#include "Mouse.h"

using namespace HerculesEngine;
using namespace HerculesEngine::GUI;

sigslot::signal2<int, int> Menu::m_onActiveMouseMove;
sigslot::signal2<int, int> Menu::m_onPassiveMouseMove;
sigslot::signal3<int, int, int> Menu::m_onMouseButtonPress;
sigslot::signal3<int, int, int> Menu::m_onMouseButtonRelease;
sigslot::signal3<int, int, int> Menu::m_onMouseWheelMove;

Menu::Menu(const std::vector<Layer*>& layers) : m_layers(layers)
{
	m_activeLayer = layers[0];
	m_active = false;

	static bool first_time = true;

	for (auto& layer : m_layers)
	{
		layer->m_onAction.connect(this, &Menu::onAction);
	}

	if (first_time == true)
	{
		MOUSE::getOnActiveMoveEvent() += onActiveMouseMoveHandler;
		MOUSE::getOnPassiveMoveEvent() += onPassiveMouseMoveHandler;
		MOUSE::getOnButtonPressEvent() += onMouseButtonPressHandler;
		MOUSE::getOnButtonReleaseEvent() += onMouseButtonReleaseHandler;
		MOUSE::getOnWheelMoveEvent() += onMouseWheelMoveHandler;
	}
}

Menu::~Menu()
{
	makeInactive();
	//INTENTIONALLY EMPTY
}

void Menu::draw()
{
	if (m_active == true && m_activeLayer != nullptr)
	{
		m_activeLayer->draw();
	}
}

void Menu::update()
{
	if (m_active == true && m_activeLayer != nullptr)
	{
		m_activeLayer->update();
	}
}

void Menu::makeActive()
{
	if (m_active == false)
	{
		m_onActiveMouseMove.connect(this, &Menu::onActiveMouseMove);
		m_onPassiveMouseMove.connect(this, &Menu::onPassiveMouseMove);
		m_onMouseButtonPress.connect(this, &Menu::onMouseButtonPress);
		m_onMouseButtonRelease.connect(this, &Menu::onMouseButtonRelease);
		m_onMouseWheelMove.connect(this, &Menu::onMouseWheelMove);

		m_active = true;
	}
}

void Menu::makeInactive()
{
	if (m_active == true)
	{
		m_onActiveMouseMove.disconnect(this);
		m_onPassiveMouseMove.disconnect(this);
		m_onMouseButtonPress.disconnect(this);
		m_onMouseButtonRelease.disconnect(this);
		m_onMouseWheelMove.disconnect(this);

		m_active = false;
	}
}

bool Menu::isActive()
{
	return m_active;
}

void Menu::onActiveMouseMoveHandler(int x, int y)
{
	m_onActiveMouseMove(x, y);
}

void Menu::onPassiveMouseMoveHandler(int x, int y)
{
	m_onPassiveMouseMove(x, y);
}

void Menu::onMouseButtonPressHandler(int button, int x, int y)
{
	m_onMouseButtonPress(button, x, y);
}

void Menu::onMouseButtonReleaseHandler(int button, int x, int y)
{
	m_onMouseButtonRelease(button, x, y);
}

void Menu::onMouseWheelMoveHandler(int direction, int x, int y)
{
	m_onMouseWheelMove(direction, x, y);
}

void Menu::onActiveMouseMove(int x, int y)
{
	m_activeLayer->onActiveMouseMove(x, y);
}

void Menu::onPassiveMouseMove(int x, int y)
{
	m_activeLayer->onPassiveMouseMove(x, y);
}

void Menu::onMouseButtonPress(int button, int x, int y)
{
	m_activeLayer->onMouseButtonPress(button, x, y);
}

void Menu::onMouseButtonRelease(int button, int x, int y)
{
	m_activeLayer->onMouseButtonRelease(button, x, y);
}

void Menu::onMouseWheelMove(int direction, int x, int y)
{
	m_activeLayer->onMouseWheelMove(direction, x, y);
}

void Menu::onAction(ACTION_DESC desc, const Action& action)
{
	printf("2");
	
	//switch (desc)
	//{
	//case ACTION_DESC::JUMP:
	//	if (action.m_jumpDestination == nullptr)
	//	{
	//		//exit menu
	//		printf("2");
	//	}
	//	else
	//	{
	//		//m_activeLayer = (Layer*)action.m_jumpDestination;
	//	}
	//	break;
	//}
}