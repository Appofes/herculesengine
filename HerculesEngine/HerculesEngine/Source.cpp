#include "Event.h"
#include "Core.h"
#include "Mouse.h"

#include <iostream>
using namespace std;

int main(int numCmdArgs, char* cmdArgs[])
{
	CORE::initialize(numCmdArgs, cmdArgs);
	//MOUSE::initialize();

	return CORE::gameLoop();
}