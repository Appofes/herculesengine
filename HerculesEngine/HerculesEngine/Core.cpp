#define __DEBUG__
#include "Core.h"
#include "Button.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "Path.h"
#include <iostream>
#include <chrono>

using namespace std;
using namespace HerculesEngine;
using namespace HerculesEngine::Logic;

int Core::m_clientWindowSize[2];
bool Core::m_paused;

Tools::Event<void, int, int> Core::m_onResize;
Tools::Event<void> Core::m_onDraw;
Tools::Event<void, float> Core::m_onUpdate;

glm::mat4x4 Core::m_projectionMatrix;
glm::mat4x4 Core::m_viewMatrix;

const int Core::INITIAL_WINDOW_SIZE[2] = { 800, 600 };
const int Core::INITIAL_PAUSE_STATE = false;
const float Core::DEFAULT_FOV_IN_DEGREES = 60.0f;
const float Core::DEFAULT_FOV_IN_RADIANS = glm::radians(Core::DEFAULT_FOV_IN_DEGREES);
const float Core::DEFAULT_NEAR_PLANE_DISTANCE = 1.0f;
const float Core::DEFAULT_FAR_PLANE_DISTANCE = 100.0f;
const glm::vec3 Core::DEFAULT_CAMERA_POSITION = glm::vec3(0.0f, 0.0f, -2.0f);
//GUI::Button* Core::m_pButton = nullptr;
GUI::Menu* Core::m_pMenu = nullptr;
Graphics::Text* Core::m_text = nullptr;

void Core::initialize(int numCmdArgs, char* cmdArgs[])
{
	auto image = stbi_load("", nullptr, nullptr, nullptr, 0);

	m_paused = INITIAL_PAUSE_STATE;
	m_clientWindowSize[0] = INITIAL_WINDOW_SIZE[0];
	m_clientWindowSize[1] = INITIAL_WINDOW_SIZE[1];

	glutInit(&numCmdArgs, cmdArgs);

	glutInitContextVersion(4, 0);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	glutInitWindowSize(INITIAL_WINDOW_SIZE[0], INITIAL_WINDOW_SIZE[1]);

	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

	auto window_handle = glutCreateWindow("HerculesEngine");

	if (window_handle < 1)
	{
		cerr << "ERROR: Could not create a new rendering window" << endl;
		exit(EXIT_FAILURE);
	}

	glutReshapeFunc(Core::onResize);
	glutDisplayFunc(Core::draw);
	glutIdleFunc(Core::onIdle);
	glutCloseFunc(Core::cleanup);

	glewExperimental = GL_TRUE;
	auto init_result = glewInit();

	if (init_result != GLEW_OK)
	{
		cerr << "ERROR: " << glewGetErrorString(init_result) << endl;
		exit(EXIT_FAILURE);
	}

#ifdef __DEBUG__
	cout << "INFO: OpenGL Version: " << glGetString(GL_VERSION) << endl;
#endif

	glGetError();

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CW);

	m_viewMatrix = glm::translate(glm::mat4x4(1.0f), DEFAULT_CAMERA_POSITION);
	m_projectionMatrix = glm::mat4x4(1.0f);

	//glutMainLoop();
	MOUSE::initialize();
	KEYBOARD::initialize();
	//GUI::Button button(glm::vec3(), vector<Graphics::VertexTextured>(), vector<UINT>(), vector<pair<string, GUI::LAYER_ELEMENT_STATE>>(), Tools::Effect("FX//GUI//VertexShader.glsl", "FX//GUI//FragmentShader.glsl"));
	//GUI::Button button;

	glm::vec3 position(0.0f, 0.6f, 0.0f);

	vector<Graphics::VertexTextured> vertices =
	{
		{ { -0.5f, -0.5f, 0.5f, 1.0f }, { 0.0f, 1.0f } },
		{ { -0.5f, 0.25f, 0.5f, 1.0f }, { 0.0f, 0.0f } },
		{ { 0.5f, 0.25f, 0.5f, 1.0f }, { 1.0f, 0.0f } },
		{ { 0.5f, -0.5f, 0.5f, 1.0f }, { 1.0f, 1.0f } }
	};

	vector<GLuint> indices =
	{
		0, 1, 2,
		0, 2, 3
	};

	vector<pair<string, GUI::LAYER_ELEMENT_STATE>> textures =
	{
		{ "Textures//GUI//default.png", GUI::LAYER_ELEMENT_STATE::DEFAULT },
		{ "Textures//GUI//focused.png", GUI::LAYER_ELEMENT_STATE::FOCUSED },
		{ "Textures//GUI//pressed.png", GUI::LAYER_ELEMENT_STATE::PRESSED }
	};

	Tools::Effect effect("FX//GUI//ButtonVertexShader.glsl", "FX//GUI//ButtonFragmentShader.glsl");

	GUI::Button button(position, vertices, indices, textures, effect);

	GUI::Action action;
	action.m_jumpDestination = nullptr;
	button.setAction(GUI::ACTION_DESC::JUMP, action);

	GUI::Button button1(glm::vec3(0.0f, -0.25f, 0.0f), vertices, indices, textures, effect);

	vector<GUI::LayerElement*> layerElements = { &button, &button1 };

	GUI::Layer layer(layerElements);

	vector<GUI::Layer*> layers = { &layer };

	//GUI::Menu menu(layers);
	GUI::Menu menu(layers);
	menu.makeActive();

	m_pMenu = &menu;
	
	Tools::Effect text_effect = Tools::Effect(Tools::Path("FX/Graphics/TextVertexShader.glsl"), Tools::Path("FX/Graphics/TextFragmentShader.glsl"));
	int fontSize[2] = { 0, 48 };
	Graphics::Text text = Graphics::Text("bgothm.ttf", fontSize, text_effect, glm::vec2(0, 0.1f), glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	text.setText("Dima Volosach");

	m_text = &text;

	glutMainLoop();
	exit(EXIT_SUCCESS);
}

int Core::gameLoop()
{
	glutMainLoop();

	return 0;
}

Core::~Core()
{
	
	//INTENTIONALLY EMPTY
}

void Core::onResize(int width, int height)
{
	m_clientWindowSize[0] = width;
	m_clientWindowSize[1] = height;

	glViewport(0, 0, width, height);

	m_projectionMatrix = glm::perspectiveFov(DEFAULT_FOV_IN_RADIANS, (float)width, (float)height, DEFAULT_NEAR_PLANE_DISTANCE, DEFAULT_FAR_PLANE_DISTANCE);

	m_onResize(width, height);
}

void Core::draw()
{
	update();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_onDraw();
	//m_pButton->draw();
	
	
	m_pMenu->draw();
	m_text->draw();
	
	
	glutSwapBuffers();
}

void Core::update()
{
	//m_pButton->update();
	//m_pMenu->update();

	static auto cached_time = chrono::steady_clock::now();

	auto delta_time = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - cached_time).count() * 0.001f;

	cached_time = chrono::steady_clock::now();

	m_onUpdate(delta_time);
}

void Core::onIdle()
{
	glutPostRedisplay();
}

void Core::cleanup()
{
#ifdef __DEBUG__
	cout << "Cleaned up" << endl;
#endif
	
	//delete m_pButton;
	//delete m_pMenu;
}

void Core::pause()
{
	m_paused = true;
}

void Core::proceed()
{
	m_paused = false;
}

int Core::getClientWindowWidth()
{
	return m_clientWindowSize[0];
}

int Core::getClientWindowHeight()
{
	return m_clientWindowSize[1];
}

bool Core::getPauseState()
{
	return m_paused;
}

Tools::Event<void, int, int>& Core::getOnResizeEvent()
{
	return m_onResize;
}

Tools::Event<void>& Core::getOnDrawEvent()
{
	return m_onDraw;
}

Tools::Event<void, float>& Core::getOnUpdateEvent()
{
	return m_onUpdate;
}

const glm::mat4x4& Core::getViewMatrix()
{
	return m_viewMatrix;
}

const glm::mat4x4& Core::getProjectionMatrix()
{
	return m_projectionMatrix;
}