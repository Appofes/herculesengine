#ifndef __TextH__
#define __TextH__

#include "GraphicsObject2D.h"
#include <string>
#include <ft2build.h>
#include FT_FREETYPE_H

namespace HerculesEngine
{
	namespace Graphics
	{
		class Text : public GraphicsObject2D
		{
		public:
			Text(const std::string& font,
				const int fontSizeInPixels[2],
				const Tools::Effect& effect,
				const glm::vec2& origin,
				const glm::vec4& color);

			Text();

			virtual ~Text();

			void setText(const std::string& text);
			const std::string& getText() const;
			void draw();
			void update();

			static void onResize(int width, int height);
		private:
			FT_Face m_face;
			FT_GlyphSlot m_glyph;
			std::string m_text;
			unsigned int m_textureLocation;
			unsigned int m_colorLocation;
			unsigned int m_textureID;
			glm::vec4 m_color;
			glm::vec2 m_origin;

			static float m_sx;
			static float m_sy;
		};
	}
}

#endif//__TextH__