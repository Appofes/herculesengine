#include "Core.h"
#include "LayerElement.h"
#include "Mouse.h"
#include "Resources.h"

using namespace HerculesEngine;
using namespace HerculesEngine::GUI;

LayerElement::LayerElement(const glm::vec3& position,
	const std::vector<Graphics::VertexTextured>& vertices,
	const std::vector<unsigned int>& indices,
	const std::vector<std::pair<std::string, LAYER_ELEMENT_STATE>>& textures,
	const Tools::Effect& effect) : 
		m_vertices(vertices), 
		m_indices(indices)
{
	m_modelMatrix = glm::translate(glm::mat4x4(1.0f), position);

	m_effect = effect;

	for (auto& texture : textures)
	{
		m_textures.push_back({ Tools::CreateTexture2D(texture.first, GL_LINEAR, GL_LINEAR), texture.second });
	}

	m_numIndices = indices.size();

	glGenVertexArrays(1, &m_vertexArrayObjectID);
	glBindVertexArray(m_vertexArrayObjectID);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &m_vertexBufferObjectID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObjectID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (GLvoid*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (GLvoid*)sizeof(vertices[0].m_position));

	glGenBuffers(1, &m_indexBufferObjectID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferObjectID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);
	
}

LayerElement::~LayerElement()
{
	for (auto& texture : m_textures)
	{
		glDeleteTextures(1, &texture.first);
	}

	glDeleteBuffers(1, &m_vertexBufferObjectID);
	glDeleteBuffers(1, &m_indexBufferObjectID);
	glDeleteVertexArrays(1, &m_vertexArrayObjectID);
}
