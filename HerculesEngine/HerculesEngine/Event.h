#ifndef __EventH__
#define __EventH__

#include <vector>
#include <functional>

namespace HerculesEngine
{
	namespace Tools
	{
		template <class Ret, class... Args>
		class Event
		{
			using Function = Ret(*)(Args...);
		public:
			Event() {}
			~Event(){}

			Event(const Event& copyingEvent) = delete;
			Event& operator=(const Event& copyingEvent) = delete;

			Event(const Event&& movingEvent) = delete;
			Event& operator=(const Event&& movingEvent) = delete;

			void operator+=(Function function)
			{
				m_functions.push_back(function);
			}

			void operator-=(Function function)
			{
				auto iterator = find(m_functions.begin(), m_functions.end(), function);
				if (iterator != m_functions.end())
				{
					m_functions.erase(iterator);
				}
			}

			void operator()(Args... args)
			{
				for (auto& function : m_functions)
				{
					function(args...);
				}
			}

		private:
			std::vector<Function> m_functions;
		};
	}
}

#endif//__EventH__