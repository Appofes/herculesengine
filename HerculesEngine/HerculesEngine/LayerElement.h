#ifndef __LayerElementH__
#define __LayerElementH__

#include "GraphicsObject2D.h"
#include "Vertex.h"
#include "Event.h"
#include "Action.h"
#include <sigslot.h>

namespace HerculesEngine
{
	namespace GUI
	{
		enum class LAYER_ELEMENT_STATE { DEFAULT, FOCUSED, PRESSED };

		class LayerElement : public Graphics::GraphicsObject2D
		{
		public:
			LayerElement(const glm::vec3& position,
				const std::vector<Graphics::VertexTextured>& vertices,
				const std::vector<unsigned int>& indices,
				const std::vector<std::pair<std::string, LAYER_ELEMENT_STATE>>& textures,
				const Tools::Effect& effect);

			virtual ~LayerElement();
			
			virtual void draw() = 0;
			virtual void update() = 0;

			virtual void setAction(ACTION_DESC desc, const Action& action) = 0;
		protected:
			virtual void onPassiveMouseMove(int x, int y) = 0;
			virtual void onActiveMouseMove(int x, int y) = 0;
			virtual void onMouseButtonPress(int button, int x, int y) = 0;
			virtual void onMouseButtonRelease(int button, int x, int y) = 0;
			virtual void onMouseWheelMove(int direction, int x, int y) = 0;
		protected:
			//Tools::Effect m_effect;
			std::vector<std::pair<unsigned int, LAYER_ELEMENT_STATE>> m_textures;
			int m_numIndices;

			std::vector<unsigned int> m_indices;
			std::vector<Graphics::VertexTextured> m_vertices;

			Action m_action;
			sigslot::signal2<ACTION_DESC, const Action&> m_onAction;

			friend class Layer;
		};
	}
}

#endif//__LayerElementH__