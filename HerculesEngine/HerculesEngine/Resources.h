#ifndef __ResourcesH__
#define __ResourcesH__

#include <string>

namespace HerculesEngine
{
	namespace Tools
	{
		unsigned int CreateTexture2D(const std::string& filename, int minFilter, int magFilter);
	}
}

#endif//__ResourcesH__