#include "Core.h"
#include "Effect.h"
#include <fstream>
#include <vector>

using namespace HerculesEngine;
using namespace HerculesEngine::Tools;

Effect::Effect()
{
	m_ID = 0;
	m_vertexShaderID = 0;
	m_fragmentShaderID = 0;
}

Effect::Effect(const std::string& vertexShaderFilename, const std::string& fragmentShaderFilename)
{
	m_ID = glCreateProgram();

	m_vertexShaderID = loadShader(vertexShaderFilename, GL_VERTEX_SHADER);
	m_fragmentShaderID = loadShader(fragmentShaderFilename, GL_FRAGMENT_SHADER);

	glAttachShader(m_ID, m_vertexShaderID);
	glAttachShader(m_ID, m_fragmentShaderID);

	glLinkProgram(m_ID);
	//TODO: check for errors
}

Effect::~Effect()
{
	//TODO: release resources
	glDetachShader(m_ID, m_vertexShaderID);
	glDeleteShader(m_vertexShaderID);

	glDetachShader(m_ID, m_fragmentShaderID);
	glDeleteShader(m_fragmentShaderID);

	glDeleteProgram(m_ID);
}

unsigned int Effect::getID() const
{
	return m_ID;
}

unsigned int Effect::getVertexShaderID() const
{
	return m_vertexShaderID;
}

unsigned int Effect::getFragmentShaderID() const
{
	return m_fragmentShaderID;
}

unsigned int Effect::getVariableUniformLocation(const std::string& variableName) const
{
	return glGetUniformLocation(m_ID, variableName.c_str());
}

void Effect::enable() const
{
	glUseProgram(m_ID);
}

void Effect::disable() const
{
	glUseProgram(0);
}

unsigned int Effect::loadShader(const std::string& filename, unsigned int shaderType)
{
	auto in_file = std::ifstream(filename.c_str(), std::ios::binary);

	if (in_file.good() == false)
	{
		//TODO: throw open file error exception
		printf("FAILED LOADING SHADER FILE\n");
		in_file.close();
		return 0;
	}

	in_file.seekg(0, std::ios::end);
	int file_size = in_file.tellg();
	in_file.seekg(0, std::ios::beg);

	auto code = std::vector<char>(file_size + 1, 0);

	in_file.read(code.data(), file_size);

	in_file.close();

	auto shader_ID = glCreateShader(shaderType);

	auto source = code.data();

	glShaderSource(shader_ID, 1, &source, nullptr);
	glCompileShader(shader_ID);

	return shader_ID;
}