#ifndef __MouseH__
#define __MouseH__

#include "Event.h"

#define MOUSE HerculesEngine::IO::Mouse

namespace HerculesEngine
{
	namespace IO
	{
		class Mouse
		{
		private:
			Mouse() = delete;
			virtual ~Mouse();

			Mouse(const Mouse& copyingMouse) = delete;
			Mouse& operator=(const Mouse& copyingMouse) = delete;

			Mouse(const Mouse&& movingMouse) = delete;
			Mouse& operator=(const Mouse&& movingMouse) = delete;

			static void onPassiveMove(int x, int y);
			static void onActiveMove(int x, int y);
			static void onPress(int button, int state, int x, int y);
			static void onWheelMove(int wheel, int direction, int x, int y);
		public:
			static void initialize();
			static void setCursorPosition(int x, int y);
			static int getCursorPositionX();
			static int getCursorPositionY();
			static Tools::Event<void, int, int>& getOnPassiveMoveEvent();
			static Tools::Event<void, int, int>& getOnActiveMoveEvent();
			static Tools::Event<void, int, int, int>& getOnButtonPressEvent();
			static Tools::Event<void, int, int, int>& getOnButtonReleaseEvent();
			static Tools::Event<void, int, int, int>& getOnWheelMoveEvent();
		private:
			static Tools::Event<void, int, int> m_onPassiveMove;
			static Tools::Event<void, int, int> m_onActiveMove;
			static Tools::Event<void, int, int, int> m_onButtonPress;
			static Tools::Event<void, int, int, int> m_onButtonRelease;
			static Tools::Event<void, int, int, int> m_onWheelMove;
			static int m_cursorPosition[2];
		};
	}
}

#endif//__MouseH__